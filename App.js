import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { registerRootComponent } from "expo";
import Constants from "expo-constants";
import moment from "moment";

import AddAlarm from "./components/AddAlarm/AddAlarm";
import AlarmList from "./components/AlarmList/AlarmList";
import { HeaderLeft } from "./components/Header/Header";

const Stack = createStackNavigator();
const topBarHeight = Constants.statusBarHeight;
const darkBlue = "#1d2640";
const lightBlue = "#A8CDFA";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: [],
      alarm: {},
    };
  }

  /** Sorting Method */
  insertionSort = (inputArr) => {
    let length = inputArr.length;
    for (let i = 1; i < length; i++) {
      let key = inputArr[i];
      let j = i - 1;

      /** Converts string Time to number in Seconds */
      const mins = (val) => {
        const hour = val.slice(0, val.indexOf(":"));
        const min = val.slice(val.indexOf(":") + 1, val.length);

        const summ = hour * 60 + min / 60;
        return summ;
      };

      while (j >= 0 && mins(inputArr[j].time) > mins(key.time)) {
        inputArr[j + 1] = inputArr[j];
        j = j - 1;
      }
      inputArr[j + 1] = key;
    }
    return inputArr;
  };

  startAlarm = (index, time, repeat) => {
    /** Clear the Internal if it's been set up before */
    this.state.interval && clearInterval(this.state.interval);

    /** Create value from Object */
    let alarmObj = {};
    Object.getOwnPropertyNames(this.state.alarm).map((item) => {
      alarmObj[item] = this.state.alarm[item];
    });

    /** Create the specific time instance of the Alarm Object */
    alarmObj[time] = time;

    /** Add run method which checks for properties of Alarm Object if each one is equal to the current time */
    Object.defineProperty(alarmObj, "run", {
      enumerable: false,
      value: function () {
        Object.keys(alarmObj).map((item) => {
          let stateCheck;

          repeat.map((item) => {
            if (item) stateCheck = true;
          });

          if (stateCheck) {
            (repeat[moment().format("d")] &&
              moment(item, "HH:mm:ss").format("HH:mm:ss") ===
                moment().format("HH:mm:ss")) ||
              (moment(item, "D-HH:mm:ss").format("D-HH:mm:ss") ===
                moment().format("D-HH:mm:ss") &&
                console.log("TIME!!"));
          } else {
            moment(item, "HH:mm:ss").format("HH:mm:ss") ===
              moment().format("HH:mm:ss") && console.log("TIME!!");
          }
        });
      },
    });

    /** Start the Internal by setting it, and the Alarm Object as new State */
    this.setState((prev) => {
      return {
        ...prev,
        alarm: alarmObj,
        interval: setInterval(alarmObj.run, 1000),
      };
    });
  };

  stopAlarm = (index, time) => {
    /** Clear the Internal */
    clearInterval(this.state.interval);

    /** Create value from Object */
    let alarm = {};
    Object.getOwnPropertyNames(this.state.alarm).map((item) => {
      alarm[item] = this.state.alarm[item];
    });

    /** Redefining the run method is essential so the new Alarm Object will be passed as map Array */
    alarm.run = function () {
      Object.keys(alarm).map((item) => {
        moment(item, "HH:mm").format("HH:mm") === moment().format("HH:mm") &&
          console.log("TIME!!");
      });
    };

    /** Delete the specific time property */
    delete alarm[time];

    /** Start the Internal by setting it, and the Alarm Object as new State */
    this.setState((prev) => {
      return {
        ...prev,
        alarm,
        interval: setInterval(alarm.run, 1000),
      };
    });
  };

  saveAlarm = (alarm, id) => {
    console.log(alarm.repetition);
    /** Determine if it's an Edited Alarm or a New one */
    if (!Object.getOwnPropertyNames(this.state.alarm).includes(alarm.time)) {
      let list = this.state.list.concat();
      isNaN(id) ? list.push(alarm) : (list[id] = alarm);

      /** Sort the Alarms in order */
      list = this.insertionSort(list);

      /** Set the updated Alarm list and Start the Alarm */
      this.setState({ list }, () =>
        this.startAlarm(id, alarm.time, alarm.repetition)
      );
    }
  };

  deleteAlarm = (index) => {
    /** Create value of List array */
    let list = this.state.list.concat();

    /** Delete Alarm by index */
    list.splice(index, 1);

    /** Set the updated Alarm list and Stop the Alarm */
    this.setState({ list }, (index) => this.stopAlarm(index));
  };

  handleToggleAlarm = (index, time, repeat) => {
    /** Create value of List array */
    let list = this.state.list.concat();

    /** Toggle the State of the Alarm */
    list[index].state = !list[index].state;

    /** Fire Handler for Start/Stop */
    list[index].state
      ? this.startAlarm(index, time, repeat)
      : this.stopAlarm(index, time);

    /** Set the updated Alarm list */
    this.setState({ list });
  };

  render() {
    const { list } = this.state;
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              height: topBarHeight + 65,
              backgroundColor: darkBlue,
            },
          }}
        >
          <Stack.Screen
            name="Alarms"
            options={({ route }) => ({
              title: route.name,
              headerTitleStyle: styles.titleFont,
            })}
          >
            {(props) => (
              <AlarmList
                {...props}
                alarms={list.concat()}
                toggleAlarm={this.handleToggleAlarm}
              />
            )}
          </Stack.Screen>
          <Stack.Screen
            name="Add Alarm"
            options={({ route }) => ({
              title: route.name,
              headerTitleStyle: styles.titleFont,
              headerBackImage: HeaderLeft,
            })}
          >
            {(props) => (
              <AddAlarm
                {...props}
                saveAlarm={this.saveAlarm}
                deleteAlarm={this.deleteAlarm}
              />
            )}
          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  alarmHolder: {
    flex: 1,
    width: "100%",
  },
  titleFont: {
    fontSize: 24,
    color: lightBlue,
  },
});
