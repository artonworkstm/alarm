import React, { Component, useLayoutEffect } from "react";
import { View, Text, TextInput, StyleSheet, ScrollView } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";

import Alarm from "../Alarm/Alarm";
import { HeaderEdit, HeaderLeft } from "../Header/Header";

const darkBlue = "#1d2640";
const darkerBlue = "#192036";
const lightBlue = "#A8CDFA";
const days = ["S", "M", "T", "W", "T", "F", "S"];

class AddAlarm extends Alarm {
  constructor(props) {
    super(props);

    if (props.route.params) {
      this.alarm = Object.create(props.route.params.alarm);
      this.index = props.route.params.index;
    } else {
      this.alarm = this.createAlarm(moment().format("D-HH:mm"), [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ]);
    }

    const { time, repetition } = this.alarm;

    this.state = {
      hour: time.slice(time.indexOf("-") + 1, time.indexOf(":")),
      min: time.slice(time.indexOf(":") + 1, time.length),
      rep: repetition.slice(),
    };
  }

  componentDidMount() {
    const { navigation, deleteAlarm } = this.props;

    /** Set custom Header */
    navigation.setOptions({
      headerLeft: () => <HeaderLeft onPress={() => navigation.goBack()} />,
      headerRight: () =>
        !isNaN(this.index) ? (
          <HeaderEdit
            onDelete={() => deleteAlarm(this.index)}
            goBack={navigation.goBack}
          />
        ) : null,
    });
  }

  render() {
    const handleHourChange = (e) => {
      const hour = e.nativeEvent.text;
      if (hour <= 24)
        this.setState((prev) => {
          return {
            ...prev,
            hour,
          };
        });
    };

    const handleMinChange = (e) => {
      const min = e.nativeEvent.text;
      if (min < 60)
        this.setState((prev) => {
          return {
            ...prev,
            min,
          };
        });
    };

    const handleRepeatTemp = (id, set) => {
      const repetition = rep.slice();
      repetition[id] = set;
      this.setState((prev) => {
        return {
          rep: repetition,
        };
      });
    };

    const { hour, min, rep } = this.state;
    const { route, navigation, saveAlarm } = this.props;

    return (
      <LinearGradient
        style={styles.gradient}
        colors={["#3b4c80", "#283457"]}
        start={[1, 0]}
        end={[0, 1]}
      >
        <ScrollView contentContainerStyle={styles.settings}>
          <Time
            hour={hour}
            min={min}
            handleHourChange={handleHourChange}
            handleMinChange={handleMinChange}
          />
          <View style={styles.dayContainer}>
            {days.map((item, index) => (
              <Day
                name={item}
                set={rep[index]}
                setRepeat={handleRepeatTemp}
                id={index}
                key={index}
              />
            ))}
          </View>
        </ScrollView>
        <SubmitButton
          navigation={navigation}
          saveAlarm={saveAlarm}
          createAlarm={this.createAlarm}
          state={this.state}
          index={this.index}
        />
      </LinearGradient>
    );
  }
}

const Time = ({ hour, min, handleHourChange, handleMinChange }) => {
  return (
    <View style={styles.timeContainer}>
      <TextInput
        onChange={handleHourChange}
        value={hour}
        style={[styles.timeFont, { textAlign: "right" }]}
        keyboardType="number-pad"
      />
      <Text style={styles.timeFont}>{" : "}</Text>
      <TextInput
        onChange={handleMinChange}
        value={min}
        style={[styles.timeFont, { textAlign: "left" }]}
        keyboardType="number-pad"
      />
    </View>
  );
};

const SubmitButton = ({ navigation, saveAlarm, createAlarm, state, index }) => {
  return (
    <View style={styles.buttonContainer}>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <View style={styles.button}>
          <Text style={[styles.buttonFont, styles.cancelFont]}>Cancel</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          const time = String(
            moment().format("D") + "-" + state.hour + ":" + state.min
          );
          const repetition = state.rep;

          const alarm = createAlarm(time, repetition);

          saveAlarm(alarm, index);
          navigation.goBack();
        }}
      >
        <View style={[styles.button, styles.save]}>
          <Text style={styles.buttonFont}>Save</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const Day = ({ name, set, setRepeat, id }) => {
  return (
    <TouchableOpacity onPress={() => setRepeat(id, !set)}>
      <View
        style={[
          styles.day,
          { backgroundColor: set ? "#273357" : "transparent" },
        ]}
      >
        <Text style={styles.dayName}>{name}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  gradient: {
    flex: 1,
  },
  settings: {
    width: "100%",
    marginVertical: "15%",
    alignItems: "center",
    backgroundColor: darkBlue,
  },
  timeContainer: {
    flexDirection: "row",
  },
  timeFont: {
    marginTop: 10,
    fontSize: 62,
    color: lightBlue,
  },
  dayContainer: {
    marginVertical: 15,
    flexDirection: "row",
  },
  day: {
    width: 32,
    height: 32,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 5,
  },
  dayName: {
    color: lightBlue,
    fontSize: 21,
  },
  buttonContainer: {
    position: "absolute",
    width: "100%",
    bottom: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonFont: {
    fontSize: 22,
    color: lightBlue,
  },
  cancelFont: {
    color: "#7b97ba",
  },
  button: {
    width: 105,
    height: 48,
    borderRadius: 50,
    marginHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  save: {
    backgroundColor: darkBlue,
  },
});

export default AddAlarm;
