import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { Text, View, StyleSheet, Switch } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";

import { HeaderRight } from "../Header/Header";

const darkBlue = "#1d2640";
const darkerBlue = "#192036";
const lightBlue = "#A8CDFA";

const AlarmList = ({ alarms, toggleAlarm, navigation }) => {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <HeaderRight
          handleAddAlarm={() => {
            navigation.navigate("Add Alarm");
          }}
        />
      ),
    });
  });

  const title = "7:30";
  const state = true;
  return (
    <LinearGradient
      style={styles.gradient}
      colors={["#3b4c80", "#283457"]}
      start={[1, 0]}
      end={[0, 1]}
    >
      {alarms.map((item, index) => (
        <Alarm
          state={item.state}
          repeat={item.repeat}
          alarm={item}
          alarmList={alarms}
          navigate={navigation.navigate}
          handleSetAlarm={() => toggleAlarm(index, item.time, item.repetition)}
          title={item.time.slice(item.time.indexOf("-") + 1, item.time.length)}
          index={index}
          key={index}
        />
      ))}
    </LinearGradient>
  );
};

const Alarm = ({
  state,
  title,
  handleSetAlarm,
  repeat,
  navigate,
  index,
  alarm,
  alarmList,
}) => {
  const handleEdit = () => {
    const newAlarm = false;
    navigate("Add Alarm", {
      alarm,
      index,
      list: alarmList.length,
    });
  };

  return (
    <View style={styles.alarm}>
      <TouchableOpacity onPress={handleEdit}>
        <View style={styles.alarmTitle}>
          <Ionicons
            style={styles.alarmIcon}
            name="md-alarm"
            size={36}
            color={darkerBlue}
          />
          <Text style={styles.alarmFont}>{title}</Text>
        </View>
      </TouchableOpacity>
      <Switch
        thumbColor={lightBlue}
        value={state}
        onTouchStart={handleSetAlarm}
        ios_backgroundColor="#2d3a63"
        trackColor={{ false: "#2d3a63" }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  gradient: {
    flex: 1,
    alignItems: "center",
  },
  alarm: {
    width: "100%",
    height: 95,
    borderBottomColor: darkBlue,
    borderBottomWidth: 1,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: "4%",
  },
  alarmTitle: {
    flexDirection: "row",
    alignItems: "center",
  },
  alarmFont: {
    fontSize: 38,
    fontWeight: "600",
    color: darkerBlue,
  },
  alarmIcon: {
    paddingRight: "3%",
  },
});

export default AlarmList;
