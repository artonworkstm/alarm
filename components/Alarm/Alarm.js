import { Component } from "react";

export default class AddAlarm extends Component {
  createAlarm = (time, rep) => {
    return new Alarm(time, rep);
  };

  deleteAlarm = (array, index) => {
    const valueArray = array.splice();
    valueArray.splice(index, 1);
    return valueArray;
  };
}

class Alarm {
  constructor(time, repetition) {
    this.state = true;
    this.time = time;
    this.repetition = repetition;
  }

  get getState() {
    return this.state;
  }

  get getTime() {
    return this.time;
  }

  get getRepetition() {
    return this.repetition;
  }

  set setState(set) {
    this.state = set;
  }

  set setTime(time) {
    this.time = time;
  }

  set setRepetition(rep) {
    this.repetition[rep] = !this.repetition[rep];
  }
}
