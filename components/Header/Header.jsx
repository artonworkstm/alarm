import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Button } from "react-native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Constants from "expo-constants";

const topBarHeight = Constants.statusBarHeight;
const darkBlue = "#1d2640";
const lightBlue = "#A8CDFA";

export const Header = ({ title }) => (
  <View style={styles.header}>
    <Text style={styles.titleFont}>{title}</Text>
  </View>
);

export const HeaderLeft = ({ onPress }) => {
  return (
    <AntDesign
      name="leftcircleo"
      size={32}
      color={lightBlue}
      style={styles.back}
      onPress={onPress}
    />
  );
};

export const HeaderEdit = ({ onDelete, goBack }) => {
  return (
    <TouchableOpacity
      onPress={() => {
        onDelete();
        goBack();
      }}
    >
      <AntDesign
        name="delete"
        size={32}
        color={"#f27c8c"}
        style={styles.plus}
      />
    </TouchableOpacity>
  );
};

export const HeaderRight = ({ handleAddAlarm }) => {
  return (
    <TouchableOpacity onPress={handleAddAlarm}>
      <AntDesign
        name="pluscircleo"
        size={32}
        color={lightBlue}
        style={styles.plus}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: "13%",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: topBarHeight,

    backgroundColor: darkBlue,
  },
  titleFont: {
    fontSize: 24,
    color: lightBlue,
  },
  back: {
    paddingLeft: 20,
  },
  plus: {
    paddingRight: 22,
  },
});
